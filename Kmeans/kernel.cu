
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

#include <fstream>
#include <string>
#include <iostream>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/count.h>
#include <thrust/sort.h>

#define ITERATIONS 10
#define MAX_RANDOM_CORD 50
#define DEF_FANCY_CLUSTERS_DIST 30.0f
#define COLORS_COUNT 7

#define THREADS_COUNT 512

#define OUTPUT_FILE "D:\\programy\\MatLabb\\bin\\kmeans.m"

typedef struct {
	float* x;
	float* y;
	float* z;
} points;

cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);
__host__ __device__ float distance(float x1, float y1, float z1, float x2, float y2, float z2);
void kMeansSeq(unsigned int k, const float* x, const float* y, const float* z, unsigned int data_count);
void read_data(char* file, float** x, float** y, float** z, int* size);
void usage();

__global__ void OldAndWrongKMeansKernel(unsigned int k, const float *x, const float *y, const float *z, unsigned int data_count, float *c_x, float* c_y, float *c_z)
{
	extern __shared__ int shrd[];
	int* cluster_points_count = shrd;
	float* cluster_sum_x = (float*) &cluster_points_count[k];
	float* cluster_sum_y = &cluster_sum_x[k];
	float* cluster_sum_z = &cluster_sum_y[k];

	int id = blockIdx.x * blockDim.x + threadIdx.x;
	int i, j, assign;
	float minDistance, dist;

	if(id < data_count) {
		for(i = 0; i < ITERATIONS; ++i) {
			if(id < k) {
				cluster_points_count[id] = 0;
				cluster_sum_x[id] = 0.0f;
				cluster_sum_y[id] = 0.0f;
				cluster_sum_z[id] = 0.0f;
			}

			__syncthreads();

			minDistance = distance(x[id], y[id], z[id], c_x[0], c_y[0], c_z[0]);
			assign = 0;
			for (j = 1; j < k; ++j) {
				if( (dist = distance(x[id], y[id], z[id], c_x[j], c_y[j], c_z[j])) < minDistance ) {
					minDistance = dist;
					assign = j;
				}
			}

			//cluster_points_count[assign]++;
			atomicAdd(&cluster_points_count[assign], 1);
			atomicAdd(&cluster_sum_x[assign], x[id]);
			atomicAdd(&cluster_sum_y[assign], y[id]);
			atomicAdd(&cluster_sum_z[assign], z[id]);

			__syncthreads();
			// Compute the mean
			if(id < k) {
				c_x[id] = cluster_sum_x[id] / cluster_points_count[id];
				c_y[id] = cluster_sum_y[id] / cluster_points_count[id];
				c_z[id] = cluster_sum_z[id] / cluster_points_count[id];
			}
		}
	}
}

__global__ void OnlyFindCentroids(unsigned int k, const float *x, const float *y, const float *z, unsigned int data_count, float *c_x, float* c_y, float *c_z, int* assignments, int* cluster_points_count, float* cluster_sum_x, float* cluster_sum_y, float* cluster_sum_z) {

	int id = blockIdx.x * blockDim.x + threadIdx.x;
	float minDistance, dist;
	int i, j, assign;
	if(id < data_count) {
		minDistance = distance(x[id], y[id], z[id], c_x[0], c_y[0], c_z[0]);
		assign = 0;
		for (j = 1; j < k; ++j) {
			if( (dist = distance(x[id], y[id], z[id], c_x[j], c_y[j], c_z[j])) < minDistance ) {
				minDistance = dist;
				assign = j;
			}
		}

		assignments[id] = assign;
	}
}

__global__ void FindCentroids(unsigned int k, const points p, unsigned int data_count, const points cents, int* assignments, int* cluster_points_count, float* cluster_sum_x, float* cluster_sum_y, float* cluster_sum_z) {

	int id = blockIdx.x * blockDim.x + threadIdx.x;
	float minDistance, dist;
	int i, j, assign;
	if(id < data_count) {
		minDistance = distance(p.x[id], p.y[id], p.z[id], cents.x[0], cents.y[0], cents.z[0]);
		assign = 0;
		for (j = 1; j < k; ++j) {
			if( (dist = distance(p.x[id], p.y[id], p.z[id], cents.x[j], cents.y[j], cents.z[j])) < minDistance ) {
				minDistance = dist;
				assign = j;
			}
		}

		assignments[id] = assign;

		//cluster_points_count[assign]++;
		//atomicAdd(&cluster_points_count[assign], 1);
		//atomicAdd(&cluster_sum_x[assign], x[id]);
		//atomicAdd(&cluster_sum_y[assign], y[id]);
		//atomicAdd(&cluster_sum_z[assign], z[id]);
	}
}

__global__ void RecomputeCentroids(unsigned int k, points cents, int* assignments, points p, int count) {
	int id = blockIdx.x * blockDim.x + threadIdx.x;
	extern __shared__ int shared[];
	int* cluster_points_count = shared;
	float* cluster_sum_x = (float*) &cluster_points_count[k];
	float* cluster_sum_y = &cluster_sum_x[k];
	float* cluster_sum_z = &cluster_sum_y[k];
	if(id < k) {

		cluster_points_count[id] = 0;
		cluster_sum_x[id] = 0;
		cluster_sum_y[id] = 0;
		cluster_sum_z[id] = 0;

		for(int i = 0; i < count; ++i) {
			if(assignments[i] == id) {
				++cluster_points_count[id];
				cluster_sum_x[id] += p.x[assignments[i]];
				cluster_sum_y[id] += p.y[assignments[i]];
				cluster_sum_z[id] += p.z[assignments[i]];
			}
		}

		cents.x[id] = cluster_sum_x[id] / (float)cluster_points_count[id];
		cents.y[id] = cluster_sum_y[id] / (float)cluster_points_count[id];
		cents.z[id] = cluster_sum_z[id] / (float)cluster_points_count[id];
	}
}

__host__ __device__ float distance(float x1, float y1, float z1, float x2, float y2, float z2) {
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1- y2)  + (z1 - z2) * (z1 - z2));
}

void kMeansSeq(unsigned int k, const float* x, const float* y, const float* z, unsigned int data_count) {
	int it, i, j, random;
	float min, dist;
	float* c_x = (float*) calloc(k, sizeof(float));			// centroids' x's
	float* c_y = (float*) calloc(k, sizeof(float));			// centroids' s's
	float* c_z = (float*) calloc(k, sizeof(float));			// centroids' z's
	float* c_sum_x = (float*) calloc(k, sizeof(float));		// sum of x's of points assigned to centroids
	float* c_sum_y = (float*) calloc(k, sizeof(float));		// sum of y's of points assigned to centroids
	float* c_sum_z = (float*) calloc(k, sizeof(float));		// sum of z's of points assigned to centroids
	int* assign = (int*) calloc(data_count, sizeof(int));	// assign[point_index] <==> index of centroid assigned to this point
	int* c_count = (int*) calloc(k, sizeof(int));			// count of points assigned to centroid

	/* Initialize centroids */
	for(i = 0; i < k; ++i) {
		random = rand() % data_count;
		c_x[i] = x[random];
		c_y[i] = y[random];
		c_z[i] = z[random];

		c_sum_x[i] = 0.0f;
		c_sum_y[i] = 0.0f;
		c_sum_z[i] = 0.0f;

		c_count[i] = 0;
	}

	/* TODO: remove fixed count of iterations */
	for(it = 0; it < ITERATIONS; ++it) {

		/*for(i = 0; i < k; ++i) {
		printf("(%f, %f, %f)\n", c_x[i], c_y[i], c_z[i]);
		}
		printf("\n");*/

		/* For every point find and assign closest centroid */
		for(i = 0; i < data_count; ++i) {
			min = distance(x[i], y[i], z[i], c_x[0], c_y[0], c_z[0]);
			assign[i] = 0;
			for(j = 1; j < k; ++j) {
				if( (dist = distance(x[i], y[i], z[i], c_x[j], c_y[j], c_z[j])) < min) {
					min = dist;
					assign[i] = j;
				}
			}

			c_sum_x[assign[i]] += x[i];
			c_sum_y[assign[i]] += y[i];
			c_sum_z[assign[i]] += z[i];
			++c_count[assign[i]];

			//	printf("[%d](%f, %f, %f) -> [%d](%f, %f, %f)\n", i, x[i], y[i], z[i], assign[i],
			//	c_x[assign[i]], c_y[assign[i]], c_z[assign[i]]);
		}

		for(i = 0; i < k; ++i) {
			c_x[i] = c_sum_x[i] / c_count[i];
			c_y[i] = c_sum_y[i] / c_count[i];
			c_z[i] = c_sum_z[i] / c_count[i];
		}


		//for(i = 0; i < k; ++i) {
		//	printf("it: %d, c[%d]: (%f, %f, %f):\n ", it+1, i, c_x[i],c_y[i],c_z[i]);
		//	for(j = 0; j < data_count; ++j) {
		//		if(assign[j] == i) {
		//			printf("(%f, %f, %f)\n", x[j], y[j], z[j]);
		//		}
		//	}
		//	printf("--------------------------------------------\n");
		//}

		//printf("********************************************\n");
	}

	for (i = 0; i < k; i++)
	{
		printf("(%f, %f, %f)\n", c_x[i], c_y[i], c_z[i]);
	}

	printf("\n");
}

void usage() {
	fprintf(stderr, "USAGE: Kmeans.exe c/g FILE [K]\n");
	exit(EXIT_FAILURE);
}

void read_data(char* file, float** x, float** y, float** z, int* size) {
	int i=0;
	std::string line;

	std::ifstream infile(file);

	if( infile ) {
		// Get data_count
		if(getline(infile, line)) {
			int ind = line.find('=');
			std::string str = line.substr(ind+1);
			*size = std::stoi(str);

			*x = (float*) calloc(*size, sizeof(float));
			*y = (float*) calloc(*size, sizeof(float));
			*z = (float*) calloc(*size, sizeof(float));

			// Get x's
			if(getline(infile, line)) {
				str = line.substr(line.find(':') + 1);
				ind = 0;
				for(i = 0; i < *size; ++i) {
					(*x)[i] = std::stoi(str.substr(ind, str.find(',') - ind));
					ind = str.find(',', ind) + 1;
				}
			}

			// Get y's
			if(getline(infile, line)) {
				str = line.substr(line.find(':') + 1);
				ind = 0;
				for(i = 0; i < *size; ++i) {
					(*y)[i] = std::stoi(str.substr(ind, str.find(',') - ind));
					ind = str.find(',', ind) + 1;
				}
			}

			// Get z's
			if(getline(infile, line)) {
				str = line.substr(line.find(':') + 1);
				ind = 0;
				for(i = 0; i < *size; ++i) {
					(*z)[i] = std::stoi(str.substr(ind, str.find(',') - ind));
					ind = str.find(',', ind) + 1;
				}
			}
		}

		//std::cout << "h_pts.x: " << std::endl;
		//for(i = 0; i < *size; ++i) {
		//	std::cout << (*h_pts.x)[i] << " ";
		//}
		//std::cout << "\nh_pts.y " << std::endl;
		//for(i = 0; i < *size; ++i) {
		//	std::cout << (*h_pts.y)[i] << " ";
		//}
		//std::cout << "\nh_pts.z " << std::endl;
		//for(i = 0; i < *size; ++i) {
		//	std::cout << (*h_pts.z)[i] << " ";
		//}
		//std::cout << *size << std::endl;
	}

	infile.close();
}

void generate_random_data(float** x, float** y, float** z, int size) {
	int i;
	*x = (float*) malloc(size * sizeof(float));
	*y = (float*) malloc(size * sizeof(float));
	*z = (float*) malloc(size * sizeof(float));

	for (i = 0; i < size; i++)
	{
		(*x)[i] = rand() % MAX_RANDOM_CORD;
		(*y)[i] = rand() % MAX_RANDOM_CORD;
		(*z)[i] = rand() % MAX_RANDOM_CORD;
	}
}

void generate_fancy_data(float** x, float** y, float** z, int size, int clusters_count, float distance = DEF_FANCY_CLUSTERS_DIST) {
	int i, j;
	float pos = 10.0f;
	float start = 0;
	int pointsPerCluster = size / clusters_count;

	*x = (float*) calloc(size, sizeof(float));
	*y = (float*) calloc(size, sizeof(float));
	*z = (float*) calloc(size, sizeof(float));

	for(j = 0; j < clusters_count; ++j, start += distance) {
		for(i = 0; i < pointsPerCluster; ++i) {
			int ind = j * pointsPerCluster + i;
			(*x)[ind] = (rand() % (int) pos) + start;
			(*y)[ind] = (rand() % (int) pos) + start;
			(*z)[ind] = (rand() % (int) pos) + start;

			//printf("(%f, %f, %f)\n", (*h_pts.x)[ind], (*h_pts.y)[ind], (*h_pts.z)[ind]);
		}
	}

	std::cout << std::endl;
}

// MAX: 33554431
int main(int argc, char* argv[])
{
	const char colors[COLORS_COUNT] = { 'r', 'g', 'b', 'y', 'm', 'c', 'w' };
	const int array_size = 5;
	const int a[array_size] = { 1, 2, 3, 4, 5 };
	const int b[array_size] = { 10, 20, 30, 40, 50 };
	int c[array_size] = { 0 };
	int k = 2, i, j, random;
	FILE* data_file;
	std::ofstream out_file;
	clock_t start, end;
	cudaEvent_t cudaStart, stop;
	float elapsedTime;

	cudaEventCreate(&cudaStart);
	cudaEventCreate(&stop);

	//float* h_pts.x;					// x coordinates of data
	//float* h_pts.y;					// y coordinates of data
	//float* h_pts.z;					// z coordinates of data
	points h_pts;
	points d_pts;
	points h_cent;
	points d_cent;
	//float* d_h_pts.x;				// device x coordinates of data
	//float* d_pts.y;				// device y coordinates of data
	//float* d_pts.z;				// device z coordinates of data
	//float* res_c_x;				// place for centroid.x result
	//float* res_c_y;				// place for centroid.y result
	//float* res_c_z;				// place for centroid.z result
	//float* d_c_x;				// device centroid.x
	//float* d_c_y;				// device centroid.x
	//float* d_c_z;				// device centroid.x
	int* d_assignments;			// device assignments
	int size;					// size of data
	int block;

	if(argc < 3) {
		usage();
	}

	srand(time(NULL));

	if( !strcmp(argv[2], "rand") ) {
		size = atoi(argv[3]);
		//generate_random_data(&h_pts.x, &h_pts.y, &h_pts.z, size);
		k = atoi(argv[4]);
		generate_fancy_data(&h_pts.x, &h_pts.y, &h_pts.z, size, k);
	} else {
		read_data(argv[2], &h_pts.x, &h_pts.y, &h_pts.z, &size);
		k = atoi(argv[3]);
	}

	if( !strcmp(argv[1], "c") ) {
		start = clock();
		kMeansSeq(k, h_pts.x, h_pts.y, h_pts.z, size);
		end = clock();

		printf("Total time: %d ms\n", (int)(end - start));
		return 0;
	}

	h_cent.x = (float*) calloc(k, sizeof(float));
	h_cent.y = (float*) calloc(k, sizeof(float));
	h_cent.z = (float*) calloc(k, sizeof(float));

	for(i = 0; i < k; ++i) {
		random = rand() % size;
		h_cent.x[i] = h_pts.x[random];
		h_cent.y[i] = h_pts.y[random];
		h_cent.z[i] = h_pts.z[random];

		//printf("res: (%f, %f %f)\n", res_c_x[i], res_c_y[i], res_c_z[i]);
	}

	cudaMalloc((void**)&d_pts.x, size * sizeof(float));
	cudaMalloc((void**)&d_pts.y, size * sizeof(float));
	cudaMalloc((void**)&d_pts.z, size * sizeof(float));
	cudaMalloc((void**)&d_cent.x, k * sizeof(float));
	cudaMalloc((void**)&d_cent.y, k * sizeof(float));
	cudaMalloc((void**)&d_cent.z, k * sizeof(float));
	cudaMalloc((void**)&d_assignments, size * sizeof(int));

	cudaMemcpy(d_pts.x, h_pts.x, size * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_pts.y, h_pts.y, size * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_pts.z, h_pts.z, size * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_cent.x, h_cent.x, k * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_cent.y, h_cent.y, k * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_cent.z, h_cent.z, k * sizeof(float), cudaMemcpyHostToDevice);

	int* h_assignments = (int*) calloc(size, sizeof(int));
	int* counts = (int*) calloc(k, sizeof(int));
	int* d_counts;
	float* d_sum_x, *d_sum_y, *d_sum_z;

	cudaMalloc((void**) &d_counts, k*sizeof(int));
	cudaMalloc((void**) &d_sum_x, k*sizeof(float));
	cudaMalloc((void**) &d_sum_y, k*sizeof(float));
	cudaMalloc((void**) &d_sum_z, k*sizeof(float));

	cudaEventRecord(cudaStart, 0);
	for(i = 0; i < ITERATIONS; ++i) {
		cudaMemset((void*) d_counts, 0, k * sizeof(int));
		cudaMemset((void*) d_sum_x, 0, k * sizeof(float));
		cudaMemset((void*) d_sum_y, 0, k * sizeof(float));
		cudaMemset((void*) d_sum_z, 0, k * sizeof(float));
		block = size/THREADS_COUNT + 1;

		cudaDeviceSynchronize();

		FindCentroids<<<block,THREADS_COUNT>>>(k, d_pts, size, d_cent, d_assignments, d_counts, d_sum_x, d_sum_y, d_sum_z);

		cudaDeviceSynchronize();

		block = k/THREADS_COUNT + 1;
		RecomputeCentroids<<<block, THREADS_COUNT, k*sizeof(int) + 3 * k * sizeof(float)>>>(k, d_cent, d_assignments, d_pts, size);
	}
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&elapsedTime, cudaStart, stop);
	//block = (size/THREADS_COUNT) ? size/THREADS_COUNT : 1;
	//OnlyFindCentroids<<<block,THREADS_COUNT>>>(k, d_pts.x, d_pts.y, d_pts.z, size, d_c_x, d_c_y, d_c_z, d_assignments, d_counts, d_sum_x, d_sum_y, d_sum_z);

	cudaMemcpy(h_assignments, d_assignments, size * sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(h_cent.x, d_cent.x, k*sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(h_cent.y, d_cent.y, k*sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(h_cent.z, d_cent.z, k*sizeof(float), cudaMemcpyDeviceToHost);

	for(i = 0; i < k; ++i) {
		printf("(%f, %f, %f)\n", h_cent.x[i], h_cent.y[i], h_cent.z[i]);
	}
	printf("Total time: %f ms\n", elapsedTime);
	end = time(NULL);

	/*for(i = 0; i < k; ++i) {
	printf("(%f, %f, %f)\n", res_c_x[i], res_c_y[i], res_c_z[i]);
	}*/

	out_file.open(OUTPUT_FILE);

	//for(i = 0; i < size; ++i) {
	//	//if(h_assignments[i] >= 0) {
	//	printf("h_assignments[%d] = %d\t", i, h_assignments[i]);
	//	//}
	//}

	for(j = 0; j < k; ++j) {
		out_file << "x" << j << " = [";
		for(i = 0; i < size - 1; ++i) {
			if(h_assignments[i] == j) {
				out_file << h_pts.x[i] << " ";
			}
		}
		if(h_assignments[size-1] == j) {
			out_file << " " << h_pts.x[size-1] << "];\n";
		} else {
			out_file << "];\n";
		}

		out_file << "y" << j << " = [";
		for(i = 0; i < size - 1; ++i) {
			if(h_assignments[i] == j) {
				out_file << h_pts.y[i] << " ";
			}
		}
		if(h_assignments[size-1] == j) {
			out_file << " " << h_pts.y[size-1] << "];\n";
		} else {
			out_file << "];\n";
		}

		out_file << "z" << j << " = [";
		for(i = 0; i < size - 1; ++i) {
			if(h_assignments[i] == j) {
				out_file << h_pts.z[i] << " ";
			}
		}
		if(h_assignments[size-1] == j) {
			out_file << " " << h_pts.z[size-1] << "];\n";
		} else {
			out_file << "];\n";
		}
	}

	out_file << "hold on;\n";

	for(i = 0; i < k; ++i) {
		out_file << "plot3(" << "x" << i << ", y" << i << ", z" << i << ", '*" << colors[i % COLORS_COUNT] << "');\n";
		//std::cout << "plot3(" << "x" << i << ", y" << i << ", z" << i << ", '*" << colors[i % COLORS_COUNT] << "');\n";
	}

	out_file << "hold off;\nview(3);";

	out_file.close();
	cudaFree(d_pts.x);
	cudaFree(d_pts.y);
	cudaFree(d_pts.z);
	cudaFree(d_cent.x);
	cudaFree(d_cent.y);
	cudaFree(d_cent.z);
	cudaFree(d_assignments);
	cudaFree(d_counts);
	cudaFree(d_sum_x);
	cudaFree(d_sum_y);
	cudaFree(d_sum_z);

	free(h_cent.x);
	free(h_cent.y);
	free(h_cent.z);
	free(h_assignments);
	free(counts);
	free(h_pts.x);
	free(h_pts.y);
	free(h_pts.z);

	return 0;
}

// Helper function for using CUDA to add vectors in parallel.
//cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size)
//{
//	int *dev_a = 0;
//	int *dev_b = 0;
//	int *dev_c = 0;
//	cudaError_t cudaStatus;
//
//	// Choose which GPU to run on, change this on a multi-GPU sh_pts.ytem.
//	cudaStatus = cudaSetDevice(0);
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
//		goto Error;
//	}
//
//	// Allocate GPU buffers for three vectors (two input, one output)    .
//	cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMalloc failed!");
//		goto Error;
//	}
//
//	cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMalloc failed!");
//		goto Error;
//	}
//
//	cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMalloc failed!");
//		goto Error;
//	}
//
//	// Copy input vectors from host memory to GPU buffers.
//	cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMemcpy failed!");
//		goto Error;
//	}
//
//	cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMemcpy failed!");
//		goto Error;
//	}
//
//	// Launch a kernel on the GPU with one thread for each element.
//	addKernel<<<1, size>>>(dev_c, dev_a, dev_b);
//
//	// Check for any errors launching the kernel
//	cudaStatus = cudaGetLastError();
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
//		goto Error;
//	}
//
//	// cudaDeviceSynchronize waits for the kernel to finish, and returns
//	// any errors encountered during the launch.
//	cudaStatus = cudaDeviceSynchronize();
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
//		goto Error;
//	}
//
//	// Copy output vector from GPU buffer to host memory.
//	cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMemcpy failed!");
//		goto Error;
//	}
//
//Error:
//	cudaFree(dev_c);
//	cudaFree(dev_a);
//	cudaFree(dev_b);
//
//	return cudaStatus;
//}