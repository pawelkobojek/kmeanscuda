
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

#include <fstream>
#include <string>
#include <iostream>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/count.h>
#include <thrust/sort.h>

#define ITERATIONS 10
#define MAX_RANDOM_CORD 50
#define DEF_FANCY_CLUSTERS_DIST 300.0f

#define OUTPUT_FILE "H:\\Windows7\\Documents\\MATLAB\\kmeans.m"

cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);
__host__ __device__ float distance(float x1, float y1, float z1, float x2, float y2, float z2);
void kMeansSeq(unsigned int k, const float* x, const float* y, const float* z, unsigned int data_count);
void read_data(char* file, float** xs, float** ys, float** zs, int* size);
void usage();

__global__ void OldAndWrongKMeansKernel(unsigned int k, const float *x, const float *y, const float *z, unsigned int data_count,
										float *c_x, float* c_y, float *c_z)
{
	extern __shared__ int shrd[];
	int* cluster_points_count = shrd;
	float* cluster_sum_x = (float*) &cluster_points_count[k];
	float* cluster_sum_y = &cluster_sum_x[k];
	float* cluster_sum_z = &cluster_sum_y[k];

	int id = blockIdx.x * 512 + threadIdx.x;
	int i, j, assign;
	float minDistance, dist;

	if(id < data_count) {
		for(i = 0; i < ITERATIONS; ++i) {
			if(id < k) {
				cluster_points_count[id] = 0;
				cluster_sum_x[id] = 0.0f;
				cluster_sum_y[id] = 0.0f;
				cluster_sum_z[id] = 0.0f;
			}

			__syncthreads();

			minDistance = distance(x[id], y[id], z[id], c_x[0], c_y[0], c_z[0]);
			assign = 0;
			for (j = 1; j < k; ++j) {
				if( (dist = distance(x[id], y[id], z[id], c_x[j], c_y[j], c_z[j])) < minDistance ) {
					minDistance = dist;
					assign = j;
				}
			}

			//cluster_points_count[assign]++;
			atomicAdd(&cluster_points_count[assign], 1);
			atomicAdd(&cluster_sum_x[assign], x[id]);
			atomicAdd(&cluster_sum_y[assign], y[id]);
			atomicAdd(&cluster_sum_z[assign], z[id]);

			__syncthreads();
			// Compute the mean
			if(id < k) {
				c_x[id] = cluster_sum_x[id] / cluster_points_count[id];
				c_y[id] = cluster_sum_y[id] / cluster_points_count[id];
				c_z[id] = cluster_sum_z[id] / cluster_points_count[id];
			}
		}
	}
}

__global__ void FindCentroids(unsigned int k, const float *x, const float *y, const float *z, unsigned int data_count, float *c_x, float* c_y, float *c_z, int* assignments, int* cluster_points_count, float* cluster_sum_x,
				float* cluster_sum_y, float* cluster_sum_z) {

	int id = blockIdx.x * blockDim.x + threadIdx.x;
	float minDistance, dist;
	int i, j, assign;
	if(id < data_count) {
		minDistance = distance(x[id], y[id], z[id], c_x[0], c_y[0], c_z[0]);
		assign = 0;
		for (j = 1; j < k; ++j) {
			if( (dist = distance(x[id], y[id], z[id], c_x[j], c_y[j], c_z[j])) < minDistance ) {
				minDistance = dist;
				assign = j;
			}
		}

		assignments[id] = assign;

		//cluster_points_count[assign]++;
		atomicAdd(&cluster_points_count[assign], 1);
		atomicAdd(&cluster_sum_x[assign], x[id]);
		atomicAdd(&cluster_sum_y[assign], y[id]);
		atomicAdd(&cluster_sum_z[assign], z[id]);
	}
}

__global__ void RecomputeCentroids(unsigned int k, float *c_x, float* c_y, float *c_z, float* cluster_sum_x, float* cluster_sum_y, float* cluster_sum_z, int* cluster_points_count) {
	int id = blockIdx.x * blockDim.x + threadIdx.x;

	if(id < k) {
		c_x[id] = cluster_sum_x[id] / cluster_points_count[id];
		c_y[id] = cluster_sum_y[id] / cluster_points_count[id];
		c_z[id] = cluster_sum_z[id] / cluster_points_count[id];
	}
}

__host__ __device__ float distance(float x1, float y1, float z1, float x2, float y2, float z2) {
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1- y2)  + (z1 - z2) * (z1 - z2));
}

void kMeansSeq(unsigned int k, const float* x, const float* y, const float* z, unsigned int data_count) {
	int it, i, j, random;
	float min, dist;
	float* c_x = (float*) calloc(k, sizeof(float));			// centroids' x's
	float* c_y = (float*) calloc(k, sizeof(float));			// centroids' s's
	float* c_z = (float*) calloc(k, sizeof(float));			// centroids' z's
	float* c_sum_x = (float*) calloc(k, sizeof(float));		// sum of x's of points assigned to centroids
	float* c_sum_y = (float*) calloc(k, sizeof(float));		// sum of y's of points assigned to centroids
	float* c_sum_z = (float*) calloc(k, sizeof(float));		// sum of z's of points assigned to centroids
	int* assign = (int*) calloc(data_count, sizeof(int));	// assign[point_index] <==> index of centroid assigned to this point
	int* c_count = (int*) calloc(k, sizeof(int));			// count of points assigned to centroid

	/* Initialize centroids */
	for(i = 0; i < k; ++i) {
		random = rand() % data_count;
		c_x[i] = x[random];
		c_y[i] = y[random];
		c_z[i] = z[random];

		c_sum_x[i] = 0.0f;
		c_sum_y[i] = 0.0f;
		c_sum_z[i] = 0.0f;

		c_count[i] = 0;
	}

	/* TODO: remove fixed count of iterations */
	for(it = 0; it < ITERATIONS; ++it) {

		for(i = 0; i < k; ++i) {
			printf("(%f, %f, %f)\n", c_x[i], c_y[i], c_z[i]);
		}
		printf("\n");

		/* For every point find and assign closest centroid */
		for(i = 0; i < data_count; ++i) {
			min = distance(x[i], y[i], z[i], c_x[0], c_y[0], c_z[0]);
			assign[i] = 0;
			for(j = 1; j < k; ++j) {
				if( (dist = distance(x[i], y[i], z[i], c_x[j], c_y[j], c_z[j])) < min) {
					min = dist;
					assign[i] = j;
				}
			}

			c_sum_x[assign[i]] += x[i];
			c_sum_y[assign[i]] += y[i];
			c_sum_z[assign[i]] += z[i];
			++c_count[assign[i]];

			//	printf("[%d](%f, %f, %f) -> [%d](%f, %f, %f)\n", i, x[i], y[i], z[i], assign[i],
			//	c_x[assign[i]], c_y[assign[i]], c_z[assign[i]]);
		}

		for(i = 0; i < k; ++i) {
			c_x[i] = c_sum_x[i] / c_count[i];
			c_y[i] = c_sum_y[i] / c_count[i];
			c_z[i] = c_sum_z[i] / c_count[i];
		}


		//for(i = 0; i < k; ++i) {
		//	printf("it: %d, c[%d]: (%f, %f, %f):\n ", it+1, i, c_x[i],c_y[i],c_z[i]);
		//	for(j = 0; j < data_count; ++j) {
		//		if(assign[j] == i) {
		//			printf("(%f, %f, %f)\n", x[j], y[j], z[j]);
		//		}
		//	}
		//	printf("--------------------------------------------\n");
		//}

		//printf("********************************************\n");
	}

	for (i = 0; i < k; i++)
	{
		printf("(%f, %f, %f)\n", c_x[i], c_y[i], c_z[i]);
	}

	printf("\n");
}

void usage() {
	fprintf(stderr, "USAGE: Kmeans.exe c/g FILE [K]\n");
	exit(EXIT_FAILURE);
}

void read_data(char* file, float** xs, float** ys, float** zs, int* size) {
	int i=0;
	std::string line;

	std::ifstream infile(file);

	if( infile ) {
		// Get data_count
		if(getline(infile, line)) {
			int ind = line.find('=');
			std::string str = line.substr(ind+1);
			*size = std::stoi(str);

			*xs = (float*) calloc(*size, sizeof(float));
			*ys = (float*) calloc(*size, sizeof(float));
			*zs = (float*) calloc(*size, sizeof(float));

			// Get x's
			if(getline(infile, line)) {
				str = line.substr(line.find(':') + 1);
				ind = 0;
				for(i = 0; i < *size; ++i) {
					(*xs)[i] = std::stoi(str.substr(ind, str.find(',') - ind));
					ind = str.find(',', ind) + 1;
				}
			}

			// Get y's
			if(getline(infile, line)) {
				str = line.substr(line.find(':') + 1);
				ind = 0;
				for(i = 0; i < *size; ++i) {
					(*ys)[i] = std::stoi(str.substr(ind, str.find(',') - ind));
					ind = str.find(',', ind) + 1;
				}
			}

			// Get z's
			if(getline(infile, line)) {
				str = line.substr(line.find(':') + 1);
				ind = 0;
				for(i = 0; i < *size; ++i) {
					(*zs)[i] = std::stoi(str.substr(ind, str.find(',') - ind));
					ind = str.find(',', ind) + 1;
				}
			}
		}

		std::cout << "xs: " << std::endl;
		for(i = 0; i < *size; ++i) {
			std::cout << (*xs)[i] << " ";
		}
		std::cout << "\nys " << std::endl;
		for(i = 0; i < *size; ++i) {
			std::cout << (*ys)[i] << " ";
		}
		std::cout << "\nzs " << std::endl;
		for(i = 0; i < *size; ++i) {
			std::cout << (*zs)[i] << " ";
		}
		std::cout << *size << std::endl;
	}

	infile.close();
}

void generate_random_data(float** xs, float** ys, float** zs, int size) {
	int i;
	*xs = (float*) malloc(size * sizeof(float));
	*ys = (float*) malloc(size * sizeof(float));
	*zs = (float*) malloc(size * sizeof(float));

	for (i = 0; i < size; i++)
	{
		(*xs)[i] = rand() % MAX_RANDOM_CORD;
		(*ys)[i] = rand() % MAX_RANDOM_CORD;
		(*zs)[i] = rand() % MAX_RANDOM_CORD;
	}
}

void generate_fancy_data(float** xs, float** ys, float** zs, int size, int clusters_count, float distance = DEF_FANCY_CLUSTERS_DIST) {
	int i, j;
	float pos = 10.0f;
	float start = 0;

	*xs = (float*) calloc(size, sizeof(float));
	*ys = (float*) calloc(size, sizeof(float));
	*zs = (float*) calloc(size, sizeof(float));

	for(j = 0; j < clusters_count; ++j, pos+=distance, start += distance) {
		for(i = 0; i < size/clusters_count; ++i) {
			(*xs)[i] = (rand() % (int) pos) + start;
			(*ys)[i] = (rand() % (int) pos) + start;
			(*zs)[i] = (rand() % (int) pos) + start;

			printf("(%f, %f, %f)\n", (*xs)[i], (*ys)[i], (*zs)[i]);
		}
	}

	std::cout << std::endl;
}

// MAX: 33554431
int main(int argc, char* argv[])
{
	const char colors[10] = { 'r', 'g', 'b', 'y', 'm', 'c', 'w', 'x' };
	const int arraySize = 5;
	const int a[arraySize] = { 1, 2, 3, 4, 5 };
	const int b[arraySize] = { 10, 20, 30, 40, 50 };
	int c[arraySize] = { 0 };
	int k = 2, i, j, random;
	FILE* data_file;
	std::ofstream out_file;
	clock_t start, end;
	cudaEvent_t cudaStart, stop;
	float elapsedTime;

	cudaEventCreate(&cudaStart);
	cudaEventCreate(&stop);

	float* xs;					// x coordinates of data
	float* ys;					// y coordinates of data
	float* zs;					// z coordinates of data
	float* d_xs;				// device x coordinates of data
	float* d_ys;				// device y coordinates of data
	float* d_zs;				// device z coordinates of data
	float* res_c_x;				// place for centroid.x result
	float* res_c_y;				// place for centroid.y result
	float* res_c_z;				// place for centroid.z result
	float* d_c_x;				// device centroid.x
	float* d_c_y;				// device centroid.x
	float* d_c_z;				// device centroid.x
	int* d_assignments;		// device assignments
	int size;					// size of data
	int block;

	if(argc < 3) {
		usage();
	}

	srand(time(NULL));

	//data_file = fopen(argv[1], "r");
	if( !strcmp(argv[2], "rand") ) {
		size = atoi(argv[3]);
		//generate_random_data(&xs, &ys, &zs, size);
		k = atoi(argv[4]);
		generate_fancy_data(&xs, &ys, &zs, size, k);
	} else {
		read_data(argv[2], &xs, &ys, &zs, &size);
		k = atoi(argv[3]);
	}

	if( !strcmp(argv[1], "c") ) {
		start = clock();
		kMeansSeq(k, xs, ys, zs, size);
		end = clock();

		printf("Total time: %d ms\n", (int)(end - start));
		return 0;
	}

	res_c_x = (float*) calloc(k, sizeof(float));
	res_c_y = (float*) calloc(k, sizeof(float));
	res_c_z = (float*) calloc(k, sizeof(float));

	for(i = 0; i < k; ++i) {
		random = rand() % size;
		res_c_x[i] = xs[rand() % size];
		res_c_y[i] = ys[rand() % size];
		res_c_z[i] = zs[rand() % size];
	}

	cudaMalloc((void**)&d_xs, size * sizeof(float));
	cudaMalloc((void**)&d_ys, size * sizeof(float));
	cudaMalloc((void**)&d_zs, size * sizeof(float));
	cudaMalloc((void**)&d_c_x, k * sizeof(float));
	cudaMalloc((void**)&d_c_y, k * sizeof(float));
	cudaMalloc((void**)&d_c_z, k * sizeof(float));
	cudaMalloc((void**)&d_assignments, size * sizeof(int));

	cudaMemcpy(d_xs, xs, size * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_ys, ys, size * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_zs, zs, size * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_c_x, res_c_x, k * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_c_y, res_c_y, k * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_c_z, res_c_z, k * sizeof(float), cudaMemcpyHostToDevice);

	cudaEventRecord(cudaStart);

	//for (int i = 0; i < size; i++)
	//{
	//	printf("(%f, %f, %f)\n", xs[i], ys[i], zs[i]);
	//}

	float* outKeys = (float*) calloc(size, sizeof(float));
	float* outValuesXs = (float*) calloc(size, sizeof(float));
	float* outValuesYs = (float*) calloc(size, sizeof(float));
	float* outValuesZs = (float*) calloc(size, sizeof(float));
	float* h_keys= (float*) calloc(size, sizeof(float));
	float* h_values = (float*) calloc(size, sizeof(float));
	int* h_assignments = (int*) calloc(size, sizeof(int));
	int* counts = (int*) calloc(k, sizeof(int));
	int* d_counts;
	float* d_sum_x, *d_sum_y, *d_sum_z;

	cudaMalloc((void**) &d_counts, k * sizeof(float));

	//thrust::device_vector<float> dv_xs;

	cudaMalloc((void**) &d_counts, k*sizeof(float));
	cudaMalloc((void**) &d_sum_x, k*sizeof(float));
	cudaMalloc((void**) &d_sum_y, k*sizeof(float));
	cudaMalloc((void**) &d_sum_z, k*sizeof(float));

	for(i = 0; i < ITERATIONS; ++i) {
		cudaMemset((void*) d_counts, 0, k * sizeof(float));
		cudaMemset((void*) d_sum_x, 0, k * sizeof(float));
		cudaMemset((void*) d_sum_y, 0, k * sizeof(float));
		cudaMemset((void*) d_sum_z, 0, k * sizeof(float));
		block = (size/512) ? size/512 : 1;

		cudaDeviceSynchronize();

		FindCentroids<<<block,512>>>(k, d_xs, d_ys, d_zs, size, d_c_x, d_c_y, d_c_z, d_assignments, d_counts, d_sum_x, d_sum_y, d_sum_z);

		cudaDeviceSynchronize();

		block = (k/512) ? k/512 : 1;
		RecomputeCentroids<<<block, 512>>>(k, d_c_x, d_c_y, d_c_z, d_sum_x, d_sum_y, d_sum_z, d_counts);
	}

	cudaEventSynchronize(stop);
	cudaEventRecord(stop);

	cudaEventElapsedTime(&elapsedTime, cudaStart, stop);

	cudaMemcpy(h_assignments, d_assignments, size * sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(res_c_x, d_c_x, k*sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(res_c_y, d_c_y, k*sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(res_c_z, d_c_z, k*sizeof(float), cudaMemcpyDeviceToHost);

	end = time(NULL);

	for(i = 0; i < k; ++i) {
		printf("(%f, %f, %f)\n", res_c_x[i], res_c_y[i], res_c_z[i]);
	}

	printf("Total time: %f ms\n", elapsedTime);

	out_file.open(OUTPUT_FILE);

	//for(i = 0; i < size; ++i) {
	//	printf("h_assignments[%d] = %d\n", i, h_assignments[i]);
	//}

	for(j = 0; j < k; ++j) {
		out_file << "x" << j << " = [";
		for(i = 0; i < size - 1; ++i) {
			if(h_assignments[i] == j) {
				out_file << xs[i] << " ";
			}
		}
		if(h_assignments[size-1] == j) {
			out_file << " " << xs[size-1] << "]\n";
		} else {
			out_file << "]\n";
		}

		out_file << "y" << j << " = [";
		for(i = 0; i < size - 1; ++i) {
			if(h_assignments[i] == j) {
				out_file << ys[i] << " ";
			}
		}
		if(h_assignments[size-1] == j) {
			out_file << " " << ys[size-1] << "]\n";
		} else {
			out_file << "]\n";
		}

		out_file << "z" << j << " = [";
		for(i = 0; i < size - 1; ++i) {
			if(h_assignments[i] == j) {
				out_file << zs[i] << " ";
			}
		}
		if(h_assignments[size-1] == j) {
			out_file << " " << zs[size-1] << "]\n";
		} else {
			out_file << "]\n";
		}
	}

	out_file << "hold on\n";

	for(i = 0; i < k; ++i) {
		out_file << "plot3(" << "x" << i << ", y" << i << ", z" << i << ", '*" << colors[i] << "')\n";
	}

	out_file << "hold off\nview(3)";

	out_file.close();
	cudaFree(d_xs);
	cudaFree(d_ys);
	cudaFree(d_zs);
	cudaFree(d_c_x);
	cudaFree(d_c_y);
	cudaFree(d_c_z);
	cudaFree(d_assignments);

	free(res_c_x);
	free(res_c_y);
	free(res_c_z);
	return 0;
}

// Helper function for using CUDA to add vectors in parallel.
//cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size)
//{
//	int *dev_a = 0;
//	int *dev_b = 0;
//	int *dev_c = 0;
//	cudaError_t cudaStatus;
//
//	// Choose which GPU to run on, change this on a multi-GPU system.
//	cudaStatus = cudaSetDevice(0);
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
//		goto Error;
//	}
//
//	// Allocate GPU buffers for three vectors (two input, one output)    .
//	cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMalloc failed!");
//		goto Error;
//	}
//
//	cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMalloc failed!");
//		goto Error;
//	}
//
//	cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMalloc failed!");
//		goto Error;
//	}
//
//	// Copy input vectors from host memory to GPU buffers.
//	cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMemcpy failed!");
//		goto Error;
//	}
//
//	cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMemcpy failed!");
//		goto Error;
//	}
//
//	// Launch a kernel on the GPU with one thread for each element.
//	addKernel<<<1, size>>>(dev_c, dev_a, dev_b);
//
//	// Check for any errors launching the kernel
//	cudaStatus = cudaGetLastError();
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
//		goto Error;
//	}
//
//	// cudaDeviceSynchronize waits for the kernel to finish, and returns
//	// any errors encountered during the launch.
//	cudaStatus = cudaDeviceSynchronize();
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
//		goto Error;
//	}
//
//	// Copy output vector from GPU buffer to host memory.
//	cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
//	if (cudaStatus != cudaSuccess) {
//		fprintf(stderr, "cudaMemcpy failed!");
//		goto Error;
//	}
//
//Error:
//	cudaFree(dev_c);
//	cudaFree(dev_a);
//	cudaFree(dev_b);
//
//	return cudaStatus;
//}
